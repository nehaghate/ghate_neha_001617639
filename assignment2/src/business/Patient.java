/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author Neha Ghate
 */
public class Patient {
    private String patientName;
    private String patientId;
    private int patientAge;
    private String doctorName;
    private String pharmacyCompany;
    private VitalSignHistory vsh;

    public VitalSignHistory getVsh() {
        return vsh;
    }

    public void setVsh(VitalSignHistory vsh) {
        this.vsh = vsh;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public int getPatientAge() {
        return patientAge;
    }

    public void setPatientAge(int patientAge) {
        this.patientAge = patientAge;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getPharmacyCompany() {
        return pharmacyCompany;
    }

    public void setPharmacyCompany(String pharmacyCompany) {
        this.pharmacyCompany = pharmacyCompany;
    }
    
    /*
    public Patient()
    {
        this.vsh=new VitalSignHistory();
    }*/
}
