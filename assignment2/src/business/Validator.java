/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;

/**
 *
 * @author Neha Ghate
 */
public class Validator {
    
    public static boolean isBetween(int x, int lower, int upper) {
             return lower <= x && x <= upper;
    }
    
    public static boolean isBetweenFloat(float x, float lower, float upper) {
             return lower <= x && x <= upper;
    }
    
     public  ArrayList<String> validateVitalSign(VitalSign vitalSignList) {
           //errorArray is used to store the error messages.
           ArrayList errorArray = new ArrayList();
           //Check is done on firstName for empty string.
           if(Float.toString(vitalSignList.getBloodPressure()).trim().equals("0.0")){
           errorArray.add("Blood Pressure");
        }
         
           if(Float.toString(vitalSignList.getRespiratoryRate()).trim().equals("0.0")){
            errorArray.add("Respiratory Rate");
        }
            if(Integer.toString(vitalSignList.getPulse()).trim().equals("0")){
            errorArray.add("Heart Rate");
        }
            if(Float.toString(vitalSignList.getWeight()).trim().equals("0.0")){
            errorArray.add("Doctor's Name");
        }
        
        return errorArray;
    }  
       
    
}
