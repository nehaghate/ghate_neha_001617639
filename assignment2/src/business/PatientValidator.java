/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Neha Ghate
 */
public class PatientValidator {

    public  ArrayList<String> validatePatient(Patient user) {
           //errorArray is used to store the error messages.
           ArrayList errorArray = new ArrayList();
         
           //Check is done on firstName for empty string.
           if(user.getPatientName().trim().equals("")){
           errorArray.add("Patient Name");
        }
         
            if(user.getPatientId().trim().equals("")){
            errorArray.add("Patient Id");
        }
            if(Integer.toString(user.getPatientAge()).trim().equals("0")){
            errorArray.add("Age");
        }
            if(user.getDoctorName().trim().equals("")){
            errorArray.add("Doctor's Name");
        }
            if(user.getPharmacyCompany().trim().equals("")){
            errorArray.add("Pharmacy company");
        }
            
        
        return errorArray;
    }
       
    }
    

