/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;

/**
 *
 * @author Neha Ghate
 */
public class VitalSignHistory {
    private ArrayList<VitalSign> vitalSignList;

    //reason?
    public VitalSignHistory() {
        this.vitalSignList = new ArrayList<>();
    }
    
    public ArrayList<VitalSign> getVitalSignList() {
        return vitalSignList;
    }

    public void setVitalSignList(ArrayList<VitalSign> vitalSignList) {
        this.vitalSignList = vitalSignList;
    }

    public VitalSign addVitalSigns()
    {
        VitalSign v = new VitalSign();
        vitalSignList.add(v);
        return v;
    }
    public ArrayList<String> getVitalSignStatus(int age, ArrayList<VitalSign> vList) {
     ArrayList statusList = new ArrayList();
     String status = "";
     
    // statusList = new ArrayList(){{add("");add("");}};
     
     for(VitalSign vitalSign : vList)     {
     
         
         if (Validator.isBetween(age, VitalSign.lowRangeAgeToddler, VitalSign.upperRangeAgeToddler)
                 && Validator.isBetweenFloat(vitalSign.getBloodPressure(), VitalSign.lowBloodPressure, VitalSign.upperBloodPressure)
                 && Validator.isBetween(vitalSign.getPulse(), 80, 130)
                 && Validator.isBetweenFloat(vitalSign.getRespiratoryRate(), 20, 30)
                 && Validator.isBetweenFloat(vitalSign.getWeight(), 22, 31))
         {
             statusList.add("Normal");
             
         }
         else
         if (Validator.isBetween(age, VitalSign.lowRangeAgePreSchool, VitalSign.upperRangePreSchool)
                 && Validator.isBetweenFloat(vitalSign.getBloodPressure(), VitalSign.lowBloodPressure, VitalSign.upperBloodPressure)
                 && Validator.isBetween(vitalSign.getPulse(),80, 120)
                 && Validator.isBetweenFloat(vitalSign.getRespiratoryRate(), VitalSign.lowRespiratoryRate, VitalSign.upperRespiratoryRate)
                 && Validator.isBetweenFloat(vitalSign.getWeight(), 31, 40))
         {
             statusList.add("Normal");
         }
         else
         if (Validator.isBetween(age, VitalSign.lowRangeAgeSchool, VitalSign.upperRangeAgeSchool)
                 && Validator.isBetweenFloat(vitalSign.getBloodPressure(), VitalSign.lowBloodPressure, VitalSign.upperBloodPressureSchoolAdolescent)
                 && Validator.isBetween(vitalSign.getPulse(), 70, 110)
                 && Validator.isBetweenFloat(vitalSign.getRespiratoryRate(), VitalSign.lowRespiratoryRate, VitalSign.upperRespiratoryRate)
                 && Validator.isBetweenFloat(vitalSign.getWeight(), 41, 92))
         {
             statusList.add("Normal");
         }
         else
         if (age >= 13 && Validator.isBetweenFloat(vitalSign.getBloodPressure(), VitalSign.lowBloodPressureAdolescent, VitalSign.upperBloodPressureSchoolAdolescent)
                 && Validator.isBetween(vitalSign.getPulse(), 55, 105)
                 && Validator.isBetweenFloat(vitalSign.getRespiratoryRate(), VitalSign.lowRespiratoryRateAdolescent, VitalSign.upperRespiratoryRateAdolescent)
                 && vitalSign.getWeight() > 110)
         {
             //status = "Normal";
             statusList.add("Normal");
         }
     
         else{
             statusList.add("Abnormal");
             }
     }
     return statusList;
    }

    public void deleteVitalSign(VitalSign vs) 
    {
        vitalSignList.remove(vs);
    }
}
