/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author Neha Ghate
 */
public class VitalSign {
    private float respiratoryRate;
    private float bloodPressure;
    private int pulse;
    private float weight;
    private Long timestamp;
    
    private Date currentDate;

    public Date getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(Date currentDate) {
        this.currentDate = currentDate;
    }
    
    public static final int lowRangeAgeToddler = 1; 
    public static final int upperRangeAgeToddler = 3; 
    public static final int lowRangeAgePreSchool = 4; 
    public static final int upperRangePreSchool = 5; 
    public static final int lowRangeAgeSchool = 6; 
    public static final int upperRangeAgeSchool = 12; 
    public static final int lowRangeAgeAdult = 13; 
    
    public static final float lowBloodPressure = 80;
    public static final float upperBloodPressure = 110;
    public static final float lowBloodPressureAdolescent = 110;
    public static final float upperBloodPressureSchoolAdolescent = 120;
    
    public static final float lowRespiratoryRate = 20;
    public static final float upperRespiratoryRate = 30;
    public static final float lowRespiratoryRateAdolescent = 12;
    public static final float upperRespiratoryRateAdolescent = 20;
    
  /*  public static final int upperPulseToddler = 130;
    public static final int lowPulse = 80;
    public static final int lowPulseSchool = 70;
    public static final int upperPulseSchool = 110; */
    
   // public static final int UPPER_PULSE = 130;
    
    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
    

    public float getRespiratoryRate() {
        return respiratoryRate;
    }

    public void setRespiratoryRate(float respiratoryRate) {
        this.respiratoryRate = respiratoryRate;
    }

    public float getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(float bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public int getPulse() {
        return pulse;
    }

    public void setPulse(int pulse) {
        this.pulse = pulse;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        //timestamp = new Date().getTime();
      //  return "VitalSign{" + "timestamp=" + timestamp + '}';
        return String.valueOf(this.currentDate);
    }

     
}
