/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.Date;

/**
 *
 * @author Neha Ghate
 */

public class Person 
{
    //Creating getter and  setter for person object
   private String firstName ;
   private String lastName ;
   private String middleName ;
   private String dateOfBirth ;
   private String streetAddress;
   private String town ;
   private int zipCode;
   private String occupation;
   private String emailAddress;
   private int areaCodeOfPhoneNumber;
   private long phoneNumber;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String personFirstName) {
        firstName = personFirstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public int getAreaCodeOfPhoneNumber() {
        return areaCodeOfPhoneNumber;
    }

    public void setAreaCodeOfPhoneNumber(int areaCodeOfPhoneNumber) {
        this.areaCodeOfPhoneNumber = areaCodeOfPhoneNumber;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    
}
