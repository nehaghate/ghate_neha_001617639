/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Neha Ghate
 */
public class Validator {
    
    public List validateForm(String firstName, String lastName,
                                    String middleName,
                                    String dateOfBirth,
                                    String streetAddress,
                                    String town,
                                    String zipCode,
                                    String occupation,
                                    String emailAddress,
                                    String areaCodeOfPhoneNumber,String phoneNumber)
            
           
    {
           String error = "";
           //errorArray is used to store the error messages.
           ArrayList errorArray = new ArrayList();
         
           //Check is done on firstName for empty string.
           if(firstName.trim().equals("")){
           error = "First name";
           errorArray.add(error);
        }
         
            if(lastName.trim().equals("")){
            error = "Last Name";
            errorArray.add(error);
        }
            if(middleName.trim().equals("")){
            error = "Middle Name";
            errorArray.add(error);
        }
            if(zipCode.trim().equals("")){
            error = "Zip Code";
            errorArray.add(error);
        }
            if(town.trim().equals("")){
            errorArray.add("Town");
        }
            if(streetAddress.trim().equals("")){
            errorArray.add("Street address");
        }
            if(dateOfBirth.trim().equals("")){
            errorArray.add("Date of Birth");
        }
            if(occupation.trim().equals("")){
            errorArray.add("Occupation");
        }    
            if(phoneNumber.trim().equals("") || phoneNumber.length() < 10){
            
            errorArray.add("Phone number");
        }
            if((emailAddress.trim().equals("")) || (!emailAddress.contains("@"))){
            
            errorArray.add("Email Address");
        }
            if(areaCodeOfPhoneNumber.trim().equals("") || areaCodeOfPhoneNumber.trim().length() < 3){
            errorArray.add("Area code");
        }
        
        return errorArray;
    }
    
}
