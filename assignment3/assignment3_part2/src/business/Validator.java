/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Neha Ghate
 */
public class Validator {
    public static boolean isBetween(int x, int lower, int upper) {
             return lower <= x && x <= upper;
    }
    
    public static boolean isBetweenFloat(float x, float lower, float upper) {
             return lower <= x && x <= upper;
    }
    
    public List validateForm(String firstName, String lastName,
                                    String dateOfBirth,
                                    String streetAddress,
                                    String zipCode,
                                    String emailAddress,
                                    String phoneNumber)
            
           
    {
           String error = "";
           //errorArray is used to store the error messages.
           ArrayList errorArray = new ArrayList();
         
           //Check is done on firstName for empty string.
           if(firstName.trim().equals("")){
           error = "First name";
           errorArray.add(error);
        }
         
            if(lastName.trim().equals("")){
            error = "Last Name";
            errorArray.add(error);
        }
            
            if(zipCode.trim().equals("")){
            error = "Zip Code";
            errorArray.add(error);
        }
            
            if(streetAddress.trim().equals("")){
            errorArray.add("Street address");
        }
            if(dateOfBirth.trim().equals("")){
            errorArray.add("Date of Birth");
        }
             
            if(phoneNumber.trim().equals("") || phoneNumber.length() < 10){
            
            errorArray.add("Phone number");
        }
            if((emailAddress.trim().equals("")) || (!emailAddress.contains("@"))){
            
            errorArray.add("Email Address");
        }
            
        
        return errorArray;
    }

    
     public  ArrayList<String> validateVitalSign(VitalSign vitalSignList) {
           //errorArray is used to store the error messages.
           ArrayList errorArray = new ArrayList();
           //Check is done on firstName for empty string.
           if(Float.toString(vitalSignList.getBloodPressure()).trim().equals("0.0")){
           errorArray.add("Blood Pressure");
        }
         
           if(Float.toString(vitalSignList.getRespiratoryRate()).trim().equals("0.0")){
            errorArray.add("Respiratory Rate");
        }
            if(Integer.toString(vitalSignList.getPulse()).trim().equals("0")){
            errorArray.add("Heart Rate");
        }
            if(Float.toString(vitalSignList.getWeight()).trim().equals("0.0")){
            errorArray.add("Doctor's Name");
        }
        
        return errorArray;
    }
    
}
