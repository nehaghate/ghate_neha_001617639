/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author Neha Ghate
 */
public class Patient {
    
    private int patientId;
    private String doctorName;
    private String pharmacyCompany;
    private Person person;
    private VitalSignHistory vsh;
    private int age;
    private static int count = 0;
    
    public Patient(){
        person = new Person();
        vsh = new VitalSignHistory();
        count++;
        patientId = count;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
       // return "Patient{" + "person=" + person + '}';
        return (String.valueOf(this.patientId));
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getPharmacyCompany() {
        return pharmacyCompany;
    }

    public void setPharmacyCompany(String pharmacyCompany) {
        this.pharmacyCompany = pharmacyCompany;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public VitalSignHistory getVsh() {
        return vsh;
    }

    public void setVsh(VitalSignHistory vsh) {
        this.vsh = vsh;
    }
    

}
