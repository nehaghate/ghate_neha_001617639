/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;

/**
 *
 * @author Neha Ghate
 */
public class PatientValidator {
    public  ArrayList<String> validatePatient(Patient user) {
           //errorArray is used to store the error messages.
           ArrayList errorArray = new ArrayList();
         
           //Check is done on firstName for empty string.
           
           if(Integer.toString(user.getPatientId()).trim().equals("0")){
            errorArray.add("PatientID");
        }
         if(Integer.toString(user.getAge()).trim().equals("0")){
            errorArray.add("Age");
        }
            if(user.getDoctorName().trim().equals("")){
            errorArray.add("Doctor's Name");
        }
            if(user.getPharmacyCompany().trim().equals("")){
            errorArray.add("Pharmacy company");
        }
        
        return errorArray;
}
}