/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;

/**
 *
 * @author Neha Ghate
 */
public class ManageApp {
    
     
     
    public static void main(String args[])
    {

        SupplierDirectory supplierDirectory;
        
        ProductCatalog productCatalogDell;
        ProductCatalog productCatalogApple;
        ProductCatalog productCatalogHP;
        ProductCatalog productCatalogLenovo;
        ProductCatalog productCatalogToshiba;
        
        
        System.out.println("hi neha");
        supplierDirectory = new SupplierDirectory();
        
        productCatalogDell =  new ProductCatalog();
        productCatalogApple = new ProductCatalog();
        productCatalogHP =  new ProductCatalog();
        productCatalogLenovo = new ProductCatalog();
        productCatalogToshiba = new ProductCatalog();
        
        
        
        ArrayList productListDell = productCatalogDell.getProductList();
        ArrayList productListApple = productCatalogApple.getProductList();
        ArrayList productListHP = productCatalogHP.getProductList();
        ArrayList productListLenovo = productCatalogLenovo.getProductList();
        ArrayList productListToshiba = productCatalogToshiba.getProductList();
        
         productCatalogDell.setProductValues("Dell Laptop","14",580,"desc1");
         productCatalogDell.setProductValues("Dell Server","14",550,"desc2");
         productCatalogDell.setProductValues("Dell mouse","15",510,"desc3"); 
         productCatalogDell.setProductValues("Dell cover","14",580,"desc4");
         productCatalogDell.setProductValues("Dell product5","14",550,"desc5");
         productCatalogDell.setProductValues("Dell product6","15",510,"desc6"); 
         productCatalogDell.setProductValues("Dell product7","15",510,"desc7"); 
         productCatalogDell.setProductValues("Dell product8","14",580,"desc8");
         productCatalogDell.setProductValues("Dell product9","14",550,"desc9");
         productCatalogDell.setProductValues("Dell product10","15",510,"desc10"); 
         
         
         productCatalogApple.setProductValues("apple Laptop","14",580,"desc1");
         productCatalogApple.setProductValues("apple Server","14",550,"desc2");
         productCatalogApple.setProductValues("apple mouse","15",510,"desc3"); 
         productCatalogApple.setProductValues("apple cover","14",580,"desc4");
         productCatalogApple.setProductValues("apple product5","14",550,"desc5");
         productCatalogApple.setProductValues("apple product6","15",510,"desc6"); 
         productCatalogApple.setProductValues("apple product7","15",510,"desc7"); 
         productCatalogApple.setProductValues("apple product8","14",580,"desc8");
         productCatalogApple.setProductValues("apple product9","14",550,"desc9");
         productCatalogApple.setProductValues("apple product10","15",510,"desc10"); 
         
         
         productCatalogHP.setProductValues("HP Laptop","14",580,"desc1");
         productCatalogHP.setProductValues("HP Server","14",550,"desc2");
         productCatalogHP.setProductValues("HP mouse","15",510,"desc3"); 
         productCatalogHP.setProductValues("HP cover","14",580,"desc4");
         productCatalogHP.setProductValues("HP product5","14",550,"desc5");
         productCatalogHP.setProductValues("HP product6","15",510,"desc6"); 
         productCatalogHP.setProductValues("HP product7","15",510,"desc7"); 
         productCatalogHP.setProductValues("HP product8","14",580,"desc8");
         productCatalogHP.setProductValues("HP product9","14",550,"desc9");
         productCatalogHP.setProductValues("HP product10","15",510,"desc10"); 
         
         productCatalogLenovo.setProductValues("Lenovo Laptop","14",580,"desc1");
         productCatalogLenovo.setProductValues("Lenovo Server","14",550,"desc2");
         productCatalogLenovo.setProductValues("Lenovo mouse","15",510,"desc3"); 
         productCatalogLenovo.setProductValues("Lenovo cover","14",580,"desc4");
         productCatalogLenovo.setProductValues("Lenovo product5","14",550,"desc5");
         productCatalogLenovo.setProductValues("Lenovo product6","15",510,"desc6"); 
         productCatalogLenovo.setProductValues("Lenovo product7","15",510,"desc7"); 
         productCatalogLenovo.setProductValues("Lenovo product8","14",580,"desc8");
         productCatalogLenovo.setProductValues("Lenovo product9","14",550,"desc9");
         productCatalogLenovo.setProductValues("Lenovo product10","15",510,"desc10"); 
         
          productCatalogToshiba.setProductValues("Toshiba Laptop","14",580,"desc1");
         productCatalogToshiba.setProductValues("Toshiba Server","14",550,"desc2");
         productCatalogToshiba.setProductValues("Toshiba mouse","15",510,"desc3"); 
         productCatalogToshiba.setProductValues("Toshiba cover","14",580,"desc4");
         productCatalogToshiba.setProductValues("Toshiba product5","14",550,"desc5");
         productCatalogToshiba.setProductValues("Toshiba product6","15",510,"desc6"); 
         productCatalogToshiba.setProductValues("Toshiba product7","15",510,"desc7"); 
         productCatalogToshiba.setProductValues("Toshiba product8","14",580,"desc8");
         productCatalogToshiba.setProductValues("Toshiba product9","14",550,"desc9");
         productCatalogToshiba.setProductValues("Toshiba product10","15",510,"desc10"); 
         
       /*
        productListDell.add(product1);
        productListDell.add(product2);
        productListDell.add(product3);
        productListDell.add(product4);
        productListDell.add(product5);
        productListDell.add(product6);
        */
        productCatalogDell.setProductList(productListDell);
        
        productCatalogApple.setProductList(productListApple);
        productCatalogHP.setProductList(productListHP);
        
        productCatalogLenovo.setProductList(productListLenovo);
        productCatalogToshiba.setProductList(productListToshiba);
        
       /* 
        ArrayList<Product> pLDell = productCatalogDell.getProductList();
        
        ArrayList<Product> pLApple = productCatalogApple.getProductList();
        ArrayList<Product> pLHP = productCatalogHP.getProductList();
        
        ArrayList<Product> pLLenovo = productCatalogLenovo.getProductList();
        ArrayList<Product> pLToshiba = productCatalogToshiba.getProductList();
        
        
        for(Product p : pL)
        {
        System.out.println("Name :"+p.getProductName());
        System.out.println("Availibility :" + p.getProductAvailibility());
        System.out.println("Price : "+p.getPrice());
        System.out.println("Description : "+p.getProductDescription());
        System.out.println("==================");
        }
        */
          supplierDirectory.setSupplierValue("Dell",productCatalogDell);
          supplierDirectory.setSupplierValue("Apple", productCatalogApple);
          supplierDirectory.setSupplierValue("HP", productCatalogHP);
          supplierDirectory.setSupplierValue("Lenovo", productCatalogLenovo);
          supplierDirectory.setSupplierValue("Toshiba", productCatalogToshiba);

         ArrayList supplierList = supplierDirectory.getSupplierList();
         
       //  supplierList.add(supplier1);
         
         supplierDirectory.setSupplierList(supplierList);
         
         ArrayList<Supplier> sL = supplierDirectory.getSupplierList();
         
         for(Supplier s : sL)
         {
             System.out.println("Supplier Name : "+ s.getSupplierName());
             ProductCatalog p =s.getProductCatalog();
             ArrayList<Product> arrPL = p.getProductList();
             System.out.println("The list of products are :");
             System.out.println("Name :"+"\t" +"\t"+"Availibility :"+"\t"+"\t"+ "Price :($) "+"\t"+"\t" + "Description : " );
              for(Product plist : arrPL)
            {
            
            System.out.println(plist.getProductName()+"\t"+"\t"+ plist.getProductAvailibility()+"\t"+"\t"+
                    plist.getPrice()+"\t"+"\t"+ plist.getProductDescription());
           // System.out.println("Name :"+plist.getProductName());
            //System.out.println("Availibility :" + plist.getProductAvailibility());
            //System.out.println("Price : "+plist.getPrice());
            //System.out.println("Description : "+plist.getProductDescription());
            }
              
             
             System.out.println("=========================");
         }
    }
}
