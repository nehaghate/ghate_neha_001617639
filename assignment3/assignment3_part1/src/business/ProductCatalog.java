/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;

/**
 *
 * @author Neha Ghate
 */
public class ProductCatalog {
    
    private ArrayList<Product> productList;
    
    public ProductCatalog()
    {
        this.productList = new ArrayList<>();
    }

    public ArrayList<Product> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<Product> productList) {
        this.productList = productList;
    }
    
    public Product addProduct()
    {
        Product p = new Product();
        productList.add(p);
        return p;
    }
    
     
     public  void setProductValues(String name,String availibility,int price,String description)
     {
         Product p = addProduct();
         p.setProductName(name);
         p.setProductAvailibility(availibility);
         p.setPrice(price);
         p.setProductDescription(description);
         //return p;
         
     }
    
    
}
