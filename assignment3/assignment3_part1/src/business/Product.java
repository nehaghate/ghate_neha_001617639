/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author Neha Ghate
 */
public class Product {
    
    private String productName;
    
    private String productAvailibility;
    
    private String productDescription;
    
    private int price;
    

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductAvailibility() {
        return productAvailibility;
    }

    public void setProductAvailibility(String productAvailibility) {
        this.productAvailibility = productAvailibility;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public int getPrice() {
        return price;
        
    }

    public void setPrice(int price) {
        this.price = price;
    }
    
    
    
}
